#!/usr/bin/perl
use warnings;
use strict;
use File::Basename;

my $usage = "
        USAGE: Quality_control_mapping_script_cleaned_up.pl direcotry containing the concatenated fastq file

Functional summary: Quality_control_mapping_script_cleaned_up.pl combines the quality control pipeline, barcode splitting and mapping and finally transfers the final files to irods and removes them";


my @files = `ls $ARGV[0]/JD*`;

if (@files==0) {
        print "no fastq files found in $ARGV[0]";
        exit;
}

foreach my $f(@files) {
     chomp $f;
     my ( $filename, $directories, $suffix ) = fileparse( $f, "JD*" );
     print "FILENAME - $filename, DIR -  $directories, SUFFIX -  $suffix\n";
     system "perl ~/upendra_shared/other_scripts/Illumina_fastq_filter-JP.pl $f >  $f.filter.fastq";
     system "cat $f.filter.fastq | perl ~/upendra_shared/other_scripts/fastx_barcode_splitter.pl --bcfile $ARGV[0]/Blk_10_barcodefile_new.txt --bol --exact --prefix $f.Blk10_new_ --suffix .fq";
     #system "perl ~/upendra_shared/other_scripts/runBWA.pl --fq $ARGV[0]/ --threads 6 --index ~/upendra_shared/Brassica_cdna_fasta_ref/Brassica_rapa.20100722.fa --out $ARGV[0]";
     #system "iput -P -V -N 6 -T -X checkpoint $ARGV[0]/JD* /iplant/home/shared/ucd.plantbio/maloof.lab/members/upendra/RIL_GENOTYPING_PROJECT/islay.qb3.berkeley.edu/VCGSL_FTP/Sample_JD001/Project_Maloof/";
     #system "rm $ARGV[0]/JD*";
}

